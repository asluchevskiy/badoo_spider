# -*- coding: utf-8 -*-
import sys
import logging
import locale
import optparse
import pymongo
from grab.tools.work import make_work
try:
    import local_settings as settings
except ImportError:
    import settings

from BadooSpider import BadooSearchSpider, BadooPageDownloadSpider, BadooAccountRegister,\
    BadooAccountModifier, ImagesDownloader, ExportItems

db = pymongo.Connection()[settings.MONGO_DB]

def worker(account):
    BadooPageDownloadSpider(login=account['_id'], password=account['password'], thread_number=settings.THREADS).run()

if __name__ == '__main__':
    # hack for unicode console i/o
    reload(sys)
    sys.setdefaultencoding(locale.getpreferredencoding())
    if settings.DEBUG:
        logging.basicConfig(level=logging.DEBUG)

    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option('-m', '--mode', action="store", dest="mode", default='search',
        help='mode to run: search|pages|autoreg|images|export')
    (options, args) = parser.parse_args()

    if options.mode == 'search':
        BadooSearchSpider(thread_number=settings.THREADS).run()
    elif options.mode == 'pages':
        accounts = db.account.find({'is_good':True}).limit(settings.THREADS)
        for res in make_work(worker, accounts, accounts.count()):
            pass
    elif options.mode == 'autoreg':
        BadooAccountRegister(thread_number=1).run()
        BadooAccountModifier(thread_number=1).run()
    elif options.mode == 'images':
        ImagesDownloader(thread_number=settings.THREADS).run()
    elif options.mode == 'export':
#        ExportItems().export()
        ExportItems().export_json()
    else:
        parser.print_help(); exit()
