# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import pymongo
import logging
from datetime import datetime
from functions import parse_page
try:
    import local_settings as settings
except ImportError:
    import settings

def test_parse():
    db = pymongo.Connection()[settings.MONGO_DB]
    for item in db.item.find({'is_downloaded':True, 'is_parsed':False}):
        s = 'Parsing http://badoo.com/%s/' % item['badoo_id']
        logging.debug(s)
        data = parse_page(item['body'])
        db.item.update({'_id':item['_id']}, {'$set':{'is_parsed':True, 'data':data, 'mtime':datetime.now()}})

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    test_parse()
