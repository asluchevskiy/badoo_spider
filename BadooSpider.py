# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import re
import logging
import signal
import json
import pymongo
import poplib
import email
import hashlib
import os
import csv
import tarfile
import string
from datetime import datetime
from pyquery import PyQuery
from grab.spider import Spider, Task
from grab.tools.user_agent import random_user_agent
from grab.tools.captcha import CaptchaSolver
from urllib import urlencode
from urlparse import urlsplit
from time import sleep
from random import randint
from functions import get_mail_body, parse_page
try:
    import local_settings as settings
except ImportError:
    import settings

class BaseSpider(Spider):

    def shutdown(self):
        print self.render_stats()

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.should_stop = True

    def prepare(self):
        if not isinstance(self, BadooPageDownloadSpider):
            signal.signal(signal.SIGINT, self.sigint_handler)
#        signal.signal(signal.SIGINT, self.sigint_handler)
        if settings.PROXY_LIST:
            self.load_proxylist(settings.PROXY_LIST, 'text_file', auto_change=False)
        if settings.USE_CACHE:
            self.setup_cache(database=settings.MONGO_DB)
        self.db = pymongo.Connection()[settings.MONGO_DB]

    def shutdown(self):
        pass

    def build_cache_hash(self, url):
        return hashlib.sha1(url).hexdigest()

    def task_image(self, grab, task):
        """
        Скачивает изображение и кладет его на диск
        """
        if grab.response.code != 200:
            logging.error('Image not 200 OK: %s' % task.url)
            return
        ext = grab.response.headers.get('Content-Type').split('/')[-1]
        if ext not in ('jpeg',):
            logging.error('Image: wrong file ext')
            return
        _hash = self.build_cache_hash(task.url)
        path_part = '%s/%s/%s.%s' % (_hash[0], _hash[1], task.badoo_id, ext)
        path = os.path.join(settings.IMAGES_DIR, path_part)
        grab.response.save(path, create_dirs=True)
        self.db.item.update({'_id':task.id}, {'$set':{'local_img':path_part}})

class BadooSearchSpider(BaseSpider):

    initial_urls = [ 'http://eu1.badoo.com/signin/' ]
    location_id = '55_0_0' # США
#    location_id = '13_0_0' # Британия
#    location_id = '32_0_0' # Канада
#    location_id = '1_0_0' # Австралия
#    location_id = '18_0_0' # Германия
#    location_id = '45_0_0' # Новая Зеладия
#    location_id = '46_0_0' # Норвегия
#    location_id = '86_0_0' # Мальта
#    location_id = '27_0_0' # Ирландия
#    location_id = '77_0_0' # Сингапур
#    location_id = '73_0_0' # Гонконг
    age_f = 30
    age_t = 55
    gender = 'M'

    def task_initial(self, grab, task):
        self.setup_grab(reuse_cookies = True)
        pq = grab.pyquery
        rt = pq.find('input[name="rt"]').attr('value')
        params = {
            'rt':rt, 'location_id':self.location_id, 'age_f':self.age_f, 'age_t':self.age_t, 'ws':1,
            'gender[]' : self.gender,
        }
        url = 'http://eu1.badoo.com/search/?' + urlencode(params)
        yield Task('search', grab=grab.clone(url=url), location_id=self.location_id)

    last_added_page = 1

    def task_search(self, grab, task):
        data = json.loads(grab.response.body)
        pq = PyQuery('<html>' + data['html'] + '</html>')

        without_photo_count = 0
        div_list = pq.find('div.user_contact')
        if not len(div_list):
            logging.error('Пользователи кончились')
            return

        for i in range(len(div_list)):
            div = div_list.eq(i)
            url = div.find('a#uid').attr('href')
            badoo_id = urlsplit(url).path.strip('/')
            gender = div.find('p.name span').attr('class').strip()
            img = div.find('img.userpic').attr('src')
            name = div.find('a#uid span.c_name_tx').text()
            age = int(re.search(', (\d+),', div.find('p.name').text()).group(1))
            try:
                photos_count = int(div.find('span.upts_f_tx').text())
            except Exception:
                without_photo_count+=1
                continue

            item = self.db.item.find_one({'badoo_id':badoo_id})
            if not item:
                self.db.item.save({
                    'badoo_id' : badoo_id, 'url' : url, 'img' : img, 'age' : age, 'name' : name,
                    'location_id' : task.location_id, 'photos_count' : photos_count,
                    'is_parsed' : False, 'ctime' : datetime.now(), 'mtime' : datetime.now(),
                    'gender' : gender, 'is_downloaded' : False, 'local_img': None,
                })

        if without_photo_count == 10:
            logging.error('Все пользователи на странице без фоток, заканчиваем')
            return

        current_page = int(data['vars']['page'])
        if current_page == self.last_added_page:
            self.last_added_page = current_page + 100
            for i in range(100):
                params = {
                    'r': data['vars']['r'], 'location_id': data['vars']['location_id'], 'gender[]' : self.gender,
                    'age_f': data['vars']['age_f'], 'age_t': data['vars']['age_t'], 'page': current_page+(i+1), 'ws': 1,
                }
                yield Task('search', grab=grab.clone(url='http://eu1.badoo.com/search/?'+urlencode(params)),
                    location_id=task.location_id)

class BadooAccountRegister(BaseSpider):

    initial_urls = [ 'http://badoo.com/' ]

    names = list()
    emails = list()
    def prepare(self):
        super(BadooAccountRegister, self).prepare()
        with open('names.list') as f:
            for line in f.readlines():
                self.names.append(line.strip())
        with open('emails.list') as f:
            for line in f.readlines():
                email, password = line.strip().split(':')
                self.emails.append((email, password))
        self.setup_grab(log_dir='logs')
        self.setup_grab(user_agent=random_user_agent())
#        self.setup_grab(debug_post=True)

    def task_initial(self, grab, task):
        self.email, self.email_password = self.emails[randint(0, len(self.emails)-1)]
#        self.email, self.email_password = self.emails[1]
#        print self.check_mail(self.email, self.email_password)
        yield Task('signup', 'http://badoo.com/signup/')

    def task_signup(self, grab, task):
        if grab.response.body.find('Hey, what gives?') != -1:
            logging.error('Bad proxy')
            if settings.PROXY_LIST:
                self.load_proxylist(settings.PROXY_LIST, 'text_file', auto_change=False)
            yield task.clone()
            return
        pq = grab.pyquery
        post = {
            'create_profile'	: 'Let me in!',
            'email' : self.email,
            'fullname' : self.names[randint(0, len(self.names)-1)],
            'gadget_t'	: '0',
            'gender' : 'M',
            'location' : u'Москва, Россия',
            'location_id' : '50_169_2351',
            'lookfor_F' : '1',
            'day' : str(randint(1,28)),
            'month' : str(randint(1,12)),
            'year' : str(randint(1978, 1990)),
            'post' : '1',
            'rt' : pq.find('input[name="rt"]').attr('value'),
            'sec_id' : '',
        }
        g = grab.clone(reuse_cookies=True, post=post)
        yield Task('signup_submit', grab=g)

    def task_signup_submit(self, grab, task):
        pq = grab.pyquery
        if grab.search(u'Этот электронный адрес уже зарегистрирован в системе'):
            logging.error('Этот электронный адрес уже зарегистрирован в системе')
            return
        activate_url = None
        while not activate_url:
            sleep(2)
            try:
                activate_url = self.check_mail(self.email, self.email_password, mode='activate')
            except Exception:
                continue
            if self.should_stop:
                break
        yield Task('activate', grab=grab.clone(url=activate_url))

    def task_activate(self, grab, task):
        password = None
        while not password:
            sleep(2)
            try:
                password = self.check_mail(self.email, self.email_password, mode='password')
            except Exception:
                continue
            if password:
                logging.debug('Saving new account %s' % self.email)
                self.db.account.save({'_id':self.email, 'email_password':self.email_password, 'password':password})
            if self.should_stop:
                break

    def check_mail(self, login, password, mode='activate'):
        box = poplib.POP3('pop3.mail.ru')
        box.user(login)
        box.pass_(password)
        response, lst, octets = box.list()
        logging.debug('Total %s messages: %s' % (login, len(lst)) )
        result = False
        for msgnum, msgsize in [i.split() for i in lst]:
            (resp, lines, octets) = box.retr(msgnum)
            msgtext = "\n".join(lines) + "\n\n"
            message = email.message_from_string(msgtext)
            if message['from'].find('Badoo') == 0:
                body = get_mail_body(message)
                print body
                if mode == 'activate':
                    s = re.search(u'http:\/\/badoo\.com\/access.+', body)
                    if s :
                        result = s.group(0).strip()
                elif mode == 'password':
                    s = re.search(u'Пароль: (.+)', body)
                    if s:
                        result = s.group(1)
        box.quit()
        return result

class BadooAccountModifier(BaseSpider):

    def task_generator(self):
        for account in self.db.account.find():
            if account.get('is_good'):
                continue
            yield Task('signin', url='http://badoo.com/signin/', email=account['_id'], password=account['password'])

    def task_signin(self, grab, task):
        g = grab.clone(reuse_cookies=True)
        g.set_input('email', task.email)
        g.set_input('password', task.password)
        g.submit(make_request=False)
        yield Task('login', grab=g, id=task.email)

    def task_login(self, grab, task):
        pq = grab.pyquery
        badoo_url = pq.find('a[rel="profile-edit"]').attr('href')
        if not badoo_url:
            logging.error('Bad login')
            return
        badoo_id = urlsplit(badoo_url).path.strip('/')
        rt = pq.find('input[name="rt"]').attr('value')
        self.db.account.update({'_id':task.id}, {'$set':{'badoo_id':badoo_id, 'is_good':True}})
        yield Task('post', grab=grab.clone(url=badoo_url+'settings/?ws=1',
            post=dict(lang_id='3',section_id='language', rt=rt)))
        yield Task('post', grab=grab.clone(url='http://badoo.com/ws/profile-ws.phtml?ws=1', post=dict(
            rt=rt, section='about', aboutme_text=u'I am an interesting guy'
        )))
        yield Task('post', grab=grab.clone(url='http://badoo.com/ws/profile-ws.phtml?ws=1', post=dict(
            rt=rt, section='interested_in', interested_in_text=u'Interesting people'
        )))
        yield Task('post', grab=grab.clone(url='http://badoo.com/ws/profile-ws.phtml?ws=1', post = {
            'body' : 'Athletic',
            'children' : 'Never',
            'company_name' : '',
            'drinking' : 'No',
            'education' : 'University',
            'eyes' : 'Grey',
            'hair' : 'Brown',
            'income' : 'High',
            'language[en]' : 1,
            'language[ru]' : '1',
            'language_more' : '0',
            'level[en]' : 'Fluent',
            'living' : 'Alone',
            'profession' : 'Manager',
            'relationship' : 'Single',
            'rt' : rt,
            'section' : 'personal',
            'sexuality' : 'Straight',
            'smoking' : 'No',
            'system' : 'metric',
            'weight[metric]' : '70000',
        }))

    def task_post(self, grab, task):
        pass

badoo_added_ids = set()

class BadooPageDownloadSpider(BaseSpider):

    initial_urls = [ 'http://badoo.com/signin/' ]

    def __init__(self, login, password, **kwargs):
        self.login = login
        self.password = password
        super(BadooPageDownloadSpider, self).__init__(**kwargs)

    def prepare(self):
        super(BadooPageDownloadSpider, self).prepare()
        self.setup_grab(reuse_cookies=True)
#        self.setup_grab(log_dir='logs')

    def task_initial(self, grab, task):
        g = grab.clone()
        g.set_input('email', self.login)
        g.set_input('password', self.password)
        g.submit(make_request=False)
        yield Task('login', grab=g)

    def task_login(self, grab, task):
        pq = grab.pyquery
        if grab.response.body.find('Hey, what gives?') != -1:
            logging.error('Bad proxy')
            # TODO: change proxy and try again
            return
        self.setup_grab(cookies=grab.response.cookies)
        self.add_new_task()

    def add_new_task(self):
        for item in self.db.item.find({'is_downloaded':False}, timeout=False):
            url = 'http://badoo.com/%s/' % item['badoo_id']
            if item['badoo_id'] not in badoo_added_ids:
                badoo_added_ids.add(item['badoo_id'])
                t = Task('page', url=url, id=item['_id'], badoo_id=item['badoo_id'])
                self.add_task(t)
                break

    def task_page(self, grab, task):
        if grab.response.code == 404:
            logging.debug('404 Not Found: %s' % task.url)
            self.db.item.remove({'_id':task.id})
            self.add_new_task()
            return
        pq = grab.pyquery
        if not pq.find('a[rel="profile-edit"]'):
            logging.error('Слетел логин')
            self.should_stop = True
            return
        if pq.find('form.no_autoloader'):
            cs = CaptchaSolver(key=settings.ANTIGATE)
            try:
                captcha_result = cs.solve_captcha(grab.clone(), url=pq.find('img#check_code_img').attr('src'))
            except Exception, e:
                logging.error(e)
                yield task.clone(); return
            g = grab.clone()
            g.set_input('checkcode', captcha_result)
            g.submit(make_request=False)
            yield Task('page', grab=g, id=task.id, task_try_count=task.task_try_count-1, badoo_id=task.badoo_id)
            return

        img_url = pq.find('img.pf_phts_b_pht').attr('src')
        if img_url:
            yield Task('image', img_url, id=task.id, badoo_id=task.badoo_id)
        else:
            logging.error('Could not find img in page %s' % task.url)

        self.db.item.update({'_id':task.id}, {'$set':{
            'is_downloaded':True,
            'body':grab.response.body,
            'data':parse_page(grab.response.body),
            'is_parsed':True,
            'mtime':datetime.now()
        }})
        self.add_new_task()

class ImagesDownloader(BaseSpider):

    def task_generator(self):
        for item in self.db.item.find({'local_img':None, 'is_downloaded':True}, timeout=False):
            pq = PyQuery(item['body'])
            url = pq.find('img.pf_phts_b_pht').attr('src')
            if not url:
                logging.error('Could not find img in page %s' % item['url'])
                continue
            yield Task('image', url, id=item['_id'], badoo_id=item['badoo_id'])

class ExportItems(BaseSpider):

    def export(self):
        self.prepare()
        limit = 100000
        i = 0
        while True:
            items = self.db.item.find({'is_parsed': True},
                {'name':1, 'badoo_id':1, 'age':1,'data':1,'local_img':1, 'gender':1},
                timeout=False).skip(i*limit).limit(limit)
            count = 0

            logging.debug('Exporting items from %s to %s' % (i*limit, (i+1)*limit))

            f = open(settings.OUT_CSV % i, 'w+')
            tar = tarfile.open(settings.OUT_TAR % i, 'w')
            writer = csv.writer(f, delimiter=';', quoting=csv.QUOTE_ALL)
            personal_info_keys = ['Living', 'Relationship', 'Work', 'Appearance', 'Languages',
                                  'Sexuality', 'Smoking', 'Drinking', 'Education', 'Children']
            writer.writerow(['URL', 'Name', 'Gender', 'Image', 'Age', 'Location', 'About', 'Interested in'] +
                            personal_info_keys + ['Tags'])

            for item in items:
                if not item.get('local_img'):
                    continue
                data = item['data']
                writer.writerow([
                    'http://badoo.com/%s/' % item['badoo_id'],
                    item['name'],
                    item['gender'],
                    item['local_img'],
                    item['age'],
                    data['Location'],
                    data.get('About me', ''),
                    data.get('Interested in', ''),
                ] + [
                    data.get('Personal info', {}).get(key, '') for key in personal_info_keys
                ] + [string.join([
                    '%s(%s)' % (tag[0], tag[1]) for tag in data['Interests']
                ], ',')])
                tar.add(os.path.join(settings.IMAGES_DIR, item['local_img']), arcname='images/'+item['local_img'])
                count += 1

            tar.close()
            f.close()
            if not count or self.should_stop:
                break
            i += 1
#            break

    def export_json(self):
        self.prepare()
        count = 0
        i = 0
        items = self.db.item.find({'is_parsed': True},
            {'name':1, 'badoo_id':1, 'age':1,'data':1,'local_img':1, 'gender':1},
            timeout=False)
        item_list = list()
        image_list = list()
        for item in items:
            if not item.get('local_img'):
                continue

            del item['_id']
            item_list.append(item)
            image_list.append(item['local_img'])
            count += 1
            if count == 100000:
                f = open(settings.OUT_JSON % i, 'w+')
                json.dump(item_list, f, ensure_ascii=False, indent=4)
                f.close()
                tar = tarfile.open(settings.OUT_TAR % i, 'w')
                for image in image_list:
                    tar.add(os.path.join(settings.IMAGES_DIR, image), arcname='images/'+image)
                tar.close()
                del item_list; item_list = list()
                del image_list; image_list = list()
                count = 0
                i+=1
