# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
from pyquery import PyQuery
from email.iterators import typed_subpart_iterator

def get_charset(message, default="ascii"):
   """Get the message charset"""

   if message.get_content_charset():
       return message.get_content_charset()

   if message.get_charset():
       return message.get_charset()

   return default

def get_mail_body(message):
   """Get the body of the email message"""

   if message.is_multipart():
       #get the plain text version only
       text_parts = [part
                     for part in typed_subpart_iterator(message,
                                                        'text',
                                                        'plain')]
       body = []
       for part in text_parts:
           charset = get_charset(part, get_charset(message))
           body.append(unicode(part.get_payload(decode=True),
                               charset,
                               "replace"))

       return u"\n".join(body).strip()

   else: # if it is not multipart, the payload will be a string
         # representing the message body
       body = unicode(message.get_payload(decode=True),
                      get_charset(message),
                      "replace")
       return body.strip()

def get_tag_class(name):
    conv = {
        'm' : 'Music',
        'c' : 'Movies & TV Shows',
        'f' : 'Fashion & Beauty',
        's' : 'Sports',
        't' : 'Travel',
        'j' : 'Profession',
        'g' : 'Games',
        'h' : 'Hobbies',
        'b' : 'Books & Culture',
        'd' : 'Food & Drink',
        'o' : 'Other',
    }
    return conv.get(name, 'Other')

def parse_page(body):
    pq = PyQuery(body)
    div_list = pq.find('div#profile_section div.profile_info')
    data = dict()
    data['Location'] = pq.find('div#block_map h2').text()
    for i in range(len(div_list)):
        div_name = div_list.eq(i).find('h2').text()
        if div_name == 'Personal info':
            dl_list = div_list.eq(i).find('dl')
            personal_info = dict()
            for j in range(len(dl_list)):
                key = dl_list.eq(j).find('dt').text().strip(':')
                val = dl_list.eq(j).find('dd').text()
                personal_info[key] = val
            data[div_name] = personal_info
        elif div_name in ('About me', 'Interested in'):
            data[div_name] = div_list.eq(i).find('div').text()
    span_list = pq.find('div#pf_ints_ls span.ints_i')
    tag_list = list()
    for i in range(len(span_list)):
        category = span_list.eq(i).find('span.ints_ic').attr('class').replace('ints_ic','').strip(' -')
        tag = span_list.eq(i).text()
        tag_list.append(( get_tag_class(category), tag ))
    data['Interests'] = tag_list
    return data
