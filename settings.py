# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os

#PROXY_LIST = os.path.join(os.path.dirname(__file__), 'ru_proxy.list')
PROXY_LIST = None

USE_CACHE = False
MONGO_DB = 'badoo'

DEBUG = True
THREADS = 4

ANTIGATE = '' # ключ антигейта

IMAGES_DIR = './badoo/'
OUT_CSV = r'./badoo/dump%.4d.csv'
OUT_JSON = r'./badoo/dump%.4d.json'
OUT_TAR = r'./badoo/dump%.4d.tar'
